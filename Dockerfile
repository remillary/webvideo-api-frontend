FROM nginx:1.13.11-alpine

ENV SPEC_URL=http://localhost:5080/api/docs

COPY run.sh /run.sh
CMD sh run.sh
COPY dist /usr/share/nginx/html/
